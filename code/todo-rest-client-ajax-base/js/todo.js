

$(function() {

    //Partie connection et enregistrement

    // Objet Player en JS
    class Player{
        constructor(login, mdp, uri){
            this.login = login;
            this.mdp = mdp;
            this.uri = uri;
            console.log(this.uri);
        }
    }


    $("#toolsPlayer #addPlayer").on("click", saveNewPlayer);
    $('#toolsPlayer #connection').on('click',connecter);



    function saveNewPlayer(){
        var player = new Player(
            $("#login").val(),
            $("#mdp").val(),
            );
        console.log(JSON.stringify(player));
        $.ajax({
            url: "http://localhost:3000/players",
            type:'POST',
            contentType:'application/json',
            data: JSON.stringify(player),
            dataType:'json',
            success:function(msg) {
                alert('Save Success');
            },
            error:function(err){
                alert('Save Error');
            }
        })
    }

    function connecter(){
        requete = "http://localhost:3000/players";
        fetch(requete)
        .then( response => {
            if(response.ok) return response.json();
            else thrownewError('Problème ajax:'+response.status);
            }
        )
        .then(verifierJoueurs)
        .catch(onerror);
    }
    var loginCourrant;
    function verifierJoueurs(players) {
        //console.log(JSON.stringify(players));
        var connecte = false;
        for(let i=0;i<players.length;i++){
            if (players[i].login==$("#login").val() && players[i].mdp==$("#mdp").val()){
                loginCourrant=$("#login").val();
                alert('Connection Success');
                connecte = true;
                document.location.href="todo.html";
                break;
            }
                   
        }
        if(!connecte){
            alert("connection failed")
        }
    }
    

    //Partie jeu du solitaire

    window.onload=function(){affichePseudo()} 

    function affichePseudo(){
        $('#pseudo').text(loginCourrant);
        console.log(loginCourrant);
    }

    $('#generer').on('click',generePartie);
    var cardList = [];
    var deck = [];
    var defausse = [];
    var pileRes1 =[];
    var pileRes2 =[];
    var pileRes3 =[];
    var pileRes4 =[];
    var pileJeu1 =[];
    var pileJeu2 =[];
    var pileJeu3 =[];
    var pileJeu4 =[];
    var pileJeu5 =[];
    var pileJeu6 =[];
    var pileJeu7 =[];
    var carteCourante = null;
    var listeCourante = null;
    var encours= false;

    function generePartie(){
        cardList = [];
        deck = [];
        defausse = [];
        pileRes1 =[];
        pileRes2 =[];
        pileRes3 =[];
        pileRes4 =[];
        pileJeu1 =[];
        pileJeu2 =[];
        pileJeu3 =[];
        pileJeu4 =[];
        pileJeu5 =[];
        pileJeu6 =[];
        pileJeu7 =[];
        carteCourante = null;
        listeCourante = null;
        for (var i = 1;i<=13;i++){
            cardList.push(i+"-carreau")
            cardList.push(i+"-coeur")
            cardList.push(i+"-pique")
            cardList.push(i+"-trefle")
        }
        cardList.shuffle();
        console.log(cardList);


        pileJeu1.push(cardList[0]);
        console.log(pileJeu1);
        for (var i = 1;i<=2;i++){
            pileJeu2.push(cardList[i]);
        }
        console.log(pileJeu2);
        for (var i = 3;i<=5;i++){
            pileJeu3.push(cardList[i]);
        }
        console.log(pileJeu3);
        for (var i = 6;i<=9;i++){
            pileJeu4.push(cardList[i]);
        }
        console.log(pileJeu4);
        for (var i = 10;i<=14;i++){
            pileJeu5.push(cardList[i]);
        }
        console.log(pileJeu5);
        for (var i = 15;i<=20;i++){
            pileJeu6.push(cardList[i]);
        }
        console.log(pileJeu6);
        for (var i = 21;i<=27;i++){
            pileJeu7.push(cardList[i]);
        }
        console.log(pileJeu7);

        for (var i = 28;i<cardList.length;i++){
            deck.push(cardList[i]);
        }
        console.log(deck);
        majAffichage();
        encours=true;
    }


    function randomInt(mini, maxi){
         var nb = mini + (maxi+1-mini)*Math.random();
         return Math.floor(nb);
    }
    
    Array.prototype.shuffle = function(n){
         if(!n)
              n = this.length;
         if(n > 1)
         {
              var i = randomInt(0, n-1);
              var tmp = this[i];
              this[i] = this[n-1];
              this[n-1] = tmp;
              this.shuffle(n-1);
         }
    }

    function majAffichage(){
        $("#defausse").empty();
        if(defausse.length>0){
            $('#defausse').append('<img src="./images/'+defausse[defausse.length-1]+'.jpg"/>');
        }

        $("#pileres1").empty();
        if(pileRes1.length>0){
            $('#pileres1').append('<img src="./images/'+pileRes1[pileRes1.length-1]+'.jpg"/>');
        }
        $("#pileres2").empty();
        if(pileRes2.length>0){
            $('#pileres2').append('<img src="./images/'+pileRes2[pileRes2.length-1]+'.jpg"/>');
        }
        $("#pileres3").empty();
        if(pileRes3.length>0){
            $('#pileres3').append('<img src="./images/'+pileRes3[pileRes3.length-1]+'.jpg"/>');
        }
        $("#pileres4").empty();
        if(pileRes4.length>0){
            $('#pileres4').append('<img src="./images/'+pileRes4[pileRes4.length-1]+'.jpg"/>');
        }

        $("#pilejeu1").empty();
        if(pileJeu1.length>0){
            $('#pilejeu1').append('<img src="./images/'+pileJeu1[pileJeu1.length-1]+'.jpg"/>');
        }
        $("#pilejeu2").empty();
        if(pileJeu2.length>0){
            $('#pilejeu2').append('<img src="./images/'+pileJeu2[pileJeu2.length-1]+'.jpg"/>');
        }
        $("#pilejeu3").empty();
        if(pileJeu3.length>0){
            $('#pilejeu3').append('<img src="./images/'+pileJeu3[pileJeu3.length-1]+'.jpg"/>');
        }
        $("#pilejeu4").empty();
        if(pileJeu4.length>0){
            $('#pilejeu4').append('<img src="./images/'+pileJeu4[pileJeu4.length-1]+'.jpg"/>');
        }
        $("#pilejeu5").empty();
        if(pileJeu5.length>0){
            $('#pilejeu5').append('<img src="./images/'+pileJeu5[pileJeu5.length-1]+'.jpg"/>');
        }
        $("#pilejeu6").empty();
        if(pileJeu6.length>0){
            $('#pilejeu6').append('<img src="./images/'+pileJeu6[pileJeu6.length-1]+'.jpg"/>');
        }
        $("#pilejeu7").empty();
        if(pileJeu7.length>0){
            $('#pilejeu7').append('<img src="./images/'+pileJeu7[pileJeu7.length-1]+'.jpg"/>');
        }
    }

    $('#sectionPaquet').on('click',pioche);
    
    function pioche(){
        if (encours){
            if (deck.length>1){
                defausse.push(deck[0]);
                deck.shift();
                majAffichage();
            }
            else{ 
            if (deck.length==1){
                defausse.push(deck[0]);
                deck.shift();
                majAffichage();
                var d = document.getElementById("sectionPaquet");
                var paquet = document.getElementById("paquet");
                var throwawayNode = d.removeChild(paquet);
            }
            else if (deck.length==0){
                $('#sectionPaquet').append('<img id="paquet" src="./images/paquet_bis.jpg" />')
                for(i=0; i<defausse.length; i++){
                    deck.push(defausse[i]);
                }
                defausse = [];
                majAffichage();
            }
        }
    }       
    }
    var testvar = 0
    document.getElementById('defausse').onclick=function(){test(defausse);}
    document.getElementById('pileres1').onclick=function(){test(pileRes1);}
    document.getElementById('pileres2').onclick=function(){test(pileRes2);}
    document.getElementById('pileres3').onclick=function(){test(pileRes3);}
    document.getElementById('pileres4').onclick=function(){test(pileRes4);}
    document.getElementById('pilejeu1').onclick=function(){test(pileJeu1);}
    document.getElementById('pilejeu2').onclick=function(){test(pileJeu2);}
    document.getElementById('pilejeu3').onclick=function(){test(pileJeu3);}
    document.getElementById('pilejeu4').onclick=function(){test(pileJeu4);}
    document.getElementById('pilejeu5').onclick=function(){test(pileJeu5);}
    document.getElementById('pilejeu6').onclick=function(){test(pileJeu6);}
    document.getElementById('pilejeu7').onclick=function(){test(pileJeu7);}

    function test(liste){
        testvar +=1;
        console.log("test n°"+testvar,liste[liste.length-1]);
        if (carteCourante == null && liste[liste.length-1] != undefined){
            carteCourante = liste[liste.length-1]
            listeCourante = liste;
            console.log(carteCourante);
            console.log(listeCourante);
        }
        else{
            if(liste != defausse && listeCourante != null){
                liste.push(carteCourante);
                listeCourante.pop();
                carteCourante = null;
                listeCourante = null;
                majAffichage();
                if(pileRes1.length==13 && pileRes2.length==13 && pileRes3.length==13 && pileRes4.length==13){
                    alert('Vous avez gagné');
                }
            }
        }
    }
});
